$(document).ready(function () {
    $('form').submit(function (e) {
        e.preventDefault();

        let values = $(this).serializeArray();
        let inputs = {};
        $.each(values, function (k, v) {
            inputs[v.name] = v.value;
        });

        $.ajax({
            url: 'data/' + inputs['code'] + '.html',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                download(inputs['code'] + '.xml', processData(data));
            },
        });
    });
});

function download(filename, text) {
    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function processData(data) {
    let $data = $(data);
    let values = {
        name: $data.find('#geos_title .region_name1.countryName').text(),
        flag: `https://www.cia.gov/library/publications/the-world-factbook/geos/${$data.find('.flag-photo-holder .flagBox img').attr('src')}`,
        map: `https://www.cia.gov/library/publications/the-world-factbook/geos/${$data.find('.flag-photo-holder .locatorBox img').attr('src')}`,
        background: $data.find('#field-background .text').text().trim(),
        geography: {
            location: processDataSelector($data, '#field-location .category_data'),
            coordinates: processDataSelector($data, '#field-geographic-coordinates .category_data'),
            mapReferences: processDataSelector($data, '#field-map-references .category_data'),
            area: {
                total: processDataSelector($data, '#field-area .subfield-number'),
                note: processDataSelector($data, '#field-area .category'),
                rank: processDataSelector($data, '#field-area .category_data:last'),
                comparative: processDataSelector($data, '#field-area-comparative .category_data', true),
            },
            landBoundaries: processDataSelector($data, '#field-land-boundaries .subfield-number'),
            coastline: processDataSelector($data, '#field-coastline .subfield-number'),
            climate: processDataSelector($data, '#field-climate .category_data'),
            environmentIssues: processDataSelector($data, '#field-environment-current-issues .category_data'),
            note: processDataSelector($data, '#field-geography-note .category_data'),
        },
        people: {
            population: processDataSelector($data, '#field-population .category_data'),
            languages: processDataSelector($data, '#field-languages .category_data'),
            hiv: processDataSelector($data, '#field-hiv-aids-adult-prevalence-rate .category_data'),
        },
        government: {
            countryName: {
                short: processDataSelector($data, '#field-country-name .category_data:nth(1)', true),
                long: processDataSelector($data, '#field-country-name .category_data:nth(0)', true),
                etymology: processDataSelector($data, '#field-country-name .category_data:nth(2)', true),
            },
            dependencyStatus: processDataSelector($data, '#field-dependency-status .category_data'),
            capital: {
                name: processDataSelector($data, '#field-capital .category_data:nth(0)', true),
                coordinates: processDataSelector($data, '#field-capital .category_data:nth(1)', true),
                timeDifference: processDataSelector($data, '#field-capital .category_data:nth(2)', true),
                savingTime: processDataSelector($data, '#field-capital .category_data:nth(3)', true),
                etymology: processDataSelector($data, '#field-capital .category_data:nth(4)', true),
            },
            constitution: processDataSelector($data, '#field-constitution .category_data'),
            legalSystem: processDataSelector($data, '#field-legal-system .category_data'),
            executiveBranch: processDataSelector($data, '#field-executive-branch .category_data'),
            judicial: processDataSelector($data, '#field-judicial-branch .category_data'),
            reprInUSA: processDataSelector($data, '#field-diplomatic-representation-in-the-us .category_data'),
            reprFromUSA: processDataSelector($data, '#field-diplomatic-representation-from-the-us .category_data'),
            flagDescription: processDataSelector($data, '#field-flag-description .category_data'),
            nationalAnthem: processDataSelector($data, '#field-national-anthem .category_data', true),
        },
        economy: {
            overview: processDataSelector($data, '#field-economy-overview .category_data'),
            exchangeRates: processDataSelector($data, '#field-exchange-rates .category_data'),
        },
        communications: {
            broadcastMedia: processDataSelector($data, '#field-broadcast-media .category_data'),
        },
        military: {
            note: processDataSelector($data, '#field-military-note .category_data'),
        },
        transportation: {
            airports: processDataSelector($data, '#field-airports .category_data'),
            airportsWithPavedRunways: processDataSelector($data, '#field-airports-with-paved-runways .category_data'),
            roadways: processDataSelector($data, '#field-transportation-roadways .category_data'),
        },
    };

    console.log($data.find('.flag-photo-holder .flagBox img'));

    return `<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE area:area SYSTEM "area.dtd">
<area:area xmlns:area="bi-xml-bhkp-area">
    <area:name>${values.name}</area:name>
    <area:flag>${values.flag}</area:flag>
    <area:map>${values.map}</area:map>
    <area:background>${values.background}</area:background>
    <geo:geography xmlns:geo="bi-xml-bhkp-area-geo">
        <geo:location>${values.geography.location}</geo:location>
        <geo:coordinates>${values.geography.coordinates}</geo:coordinates>
        <geo:map-references>${values.geography.mapReferences}</geo:map-references>
        <geo:area>
            <geo:area-total>${values.geography.area.total}</geo:area-total>
            <geo:area-note>${values.geography.area.note}</geo:area-note>
            <geo:area-rank>${values.geography.area.rank}</geo:area-rank>
            <geo:area-comparative>${values.geography.area.comparative}</geo:area-comparative>
        </geo:area>
        <geo:land-boundaries>${values.geography.landBoundaries}</geo:land-boundaries>
        <geo:coastline>${values.geography.coastline}</geo:coastline>
        <geo:climate>${values.geography.climate}</geo:climate>
        <geo:environment-issues>${values.geography.environmentIssues}</geo:environment-issues>
        <geo:geography-note>${values.geography.note}</geo:geography-note>
    </geo:geography>
    <area:people-and-society>
        <area:population>${values.people.population}</area:population>
        <area:languages>${values.people.languages}</area:languages>
        <area:hiv>${values.people.hiv}</area:hiv>
    </area:people-and-society>
    <gov:government xmlns:gov="bi-xml-bhkp-area-government">
        <gov:country-name>
            <gov:country-name-short>${values.government.countryName.short}</gov:country-name-short>
            <gov:country-name-long>${values.government.countryName.long}</gov:country-name-long>
            <gov:etymology>${values.government.countryName.etymology}</gov:etymology>
        </gov:country-name>
        <gov:dependency-status>${values.government.dependencyStatus}</gov:dependency-status>
        <gov:capital>
            <gov:capital-name>${values.government.capital.name}</gov:capital-name>
            <gov:capital-coordinates>${values.government.capital.coordinates}</gov:capital-coordinates>
            <gov:capital-time-difference>${values.government.capital.timeDifference}</gov:capital-time-difference>
            <gov:daylight-saving-time>${values.government.capital.savingTime}</gov:daylight-saving-time>
            <gov:etymology>${values.government.capital.etymology}</gov:etymology>
        </gov:capital>
        <gov:constitution>${values.government.constitution}</gov:constitution>
        <gov:legal-system>${values.government.legalSystem}</gov:legal-system>
        <gov:executive-branch>${values.government.executiveBranch}</gov:executive-branch>
        <gov:judicial-branch>${values.government.judicial}</gov:judicial-branch>
        <gov:diplomatic-representation-in-USA>${values.government.reprInUSA}</gov:diplomatic-representation-in-USA>
        <gov:diplomatic-representation-from-USA>${values.government.reprFromUSA}</gov:diplomatic-representation-from-USA>
        <gov:flag-description>${values.government.flagDescription}</gov:flag-description>
        <gov:national-anthem>${values.government.nationalAnthem}</gov:national-anthem>
    </gov:government>
    <area:economy>
        <area:economy-overview>${values.economy.overview}</area:economy-overview>
        <area:exchange-rates>${values.economy.exchangeRates}</area:exchange-rates>
    </area:economy>
    <area:communications>
        <area:broadcast-media>${values.communications.broadcastMedia}</area:broadcast-media>
    </area:communications>
    <area:military-and-security>
        <area:military-note>${values.military.note}</area:military-note>
    </area:military-and-security>
    <area:transportation>
        <area:airports>${values.transportation.airports}</area:airports>
        <area:airports-with-paved-runways>${values.transportation.airportsWithPavedRunways}</area:airports-with-paved-runways>
        <area:roadways>${values.transportation.roadways}</area:roadways>
    </area:transportation>
</area:area>
`;
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function processDataSelector($data, selector, onlyText = false) {
    return htmlEntities($data
        .find(selector).contents()
        .filter(function () {
            return !onlyText || this.nodeType == Node.TEXT_NODE;
        }).text().trim());
}
