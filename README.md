# BI-XML

Tento projekt vznikl jako semestrální práce předmětu BI-XML v akademickém roce 2019/2020.

## Validace

Všechny XML soubory jsou "well-formed" a jejich struktura může být ověřena pomocí DTD nebo RelaxNG.

### Všechny soubory

```bash
chmod +x validation.sh
./validation.sh
```

### Soubor team.xml

```bash
xmllint --noout --dtdvalid 'team/team.dtd' 'team/team.xml'
jing -c 'team/team.rnc' 'team/team.xml'
```

### Soubory s oblastmi

```bash
xmllint --noout --dtdvalid 'area/area.dtd' 'area/{nazev_souboru}.xml'
jing -c 'area/area.rnc' 'area/{nazev_souboru}.xml'
```

## Parser

```bash
cd parser
python3 -m http.server 8080
```

Potom otevřít `localhost:8080` a parsovat (data file musí být v `parser/data`).

## Spojení všech XML dokumentů do jednoho souboru
```bash
xmllint -noent source.xml > output/project.xml
```

## Generování PDF

### FO

```bash
java -jar transformations/saxon9he.jar output/project.xml transformations/pdf/projectToPdf.xsl > output/pdf/project.fo
```

### PDF

```bash
fop -fo output/pdf/project.fo -pdf output/pdf/project.pdf
```

## Transformace do HTML

### Hlavní stránka

```bash
java -jar transformations/saxon9he.jar team/team.xml transformations/html/mainPageToHtml.xsl
```

### Jednotlivé země

```bash
java -jar transformations/saxon9he.jar output/project.xml transformations/html/countryToHtml.xsl
```