#!/bin/bash

areas=( "ax" "bt" "ce" "cr" "hk" "rs" "sn" "tw" )

printf "\n========================================================\n"
printf "Validation of team file\n"
printf "========================================================\n"

# DTD validation
xmllint --noout --dtdvalid 'team/team.dtd' 'team/team.xml'
printf "DTD validation of file team/team.xml completed\n"

# RelaxNG validation
jing -c 'team/team.rnc' 'team/team.xml'
printf "RelaxNG validation of file team/team.xml completed\n"

printf "\n========================================================\n"
printf "Validation of area files\n"
printf "========================================================\n"

# DTD validation
for area in "${areas[@]}"
do
    xmllint --noout --dtdvalid 'area/area.dtd' "area/${area}.xml"
    printf "DTD validation of file area/${area}.xml completed\n"
done

# RelaxNG validation
for area in "${areas[@]}"
do
    jing -c 'area/area.rnc' "area/${area}.xml"
    printf "RelaxNG validation of file area/${area}.xml completed\n"
done

exit 0
