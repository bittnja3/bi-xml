<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:a="bi-xml-bhkp-team"
                xmlns:area="bi-xml-bhkp-area"
                xmlns:geo="bi-xml-bhkp-area-geo"
                xmlns:gov="bi-xml-bhkp-area-government">

<xsl:output method="xml" encoding="utf-8"/>
<xsl:template match="/">
<fo:root font-family="Times New Roman, serif" font-size="11pt" language="cs" hyphenate="true">
    <!-- Definice layoutu stránky -->
    <fo:layout-master-set>
      <!-- Rozměry stránky a její okraje -->
      <fo:simple-page-master master-name="A4"
                             page-height="297mm" 
                             page-width="210mm" 
                             margin="2cm">
        <!-- Tiskové zrcadlo - oblast pro samotný obsah stránky -->
        <fo:region-body margin-bottom="10mm" margin-top="10mm"/>        
        <fo:region-before margin-bottom="10mm"/>
        <!-- Oblast pro patu stránky -->
        <fo:region-after extent="5mm"/>
      </fo:simple-page-master>
    </fo:layout-master-set>
    
    <!-- Definice obsahu stránky -->
    <fo:page-sequence master-reference="A4">
      <!-- Společný obsah všech stránek v patě stránky -->
      <fo:static-content flow-name="xsl-region-after">
        <fo:block>
          <xsl:text>Page </xsl:text>

          <fo:page-number/>
        </fo:block>
      </fo:static-content>

      <fo:static-content flow-name="xsl-region-before">
          <fo:block>
            <fo:table>
            <fo:table-column />
            <fo:table-column />

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell>
                  <fo:block>
                    <xsl:text>
                      Semestral Work BI-XML B191
                    </xsl:text>  
                  </fo:block>
                </fo:table-cell>

                <fo:table-cell>
                  <fo:block text-align="right">
                    <fo:basic-link internal-destination="info" text-decoration="underline">
                      <xsl:text>
                        Back to Contents And Project Information
                      </xsl:text>
                    </fo:basic-link>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
        </fo:block>
      </fo:static-content>

      <!-- Samotný text dokumentu -->
      <fo:flow flow-name="xsl-region-body">
        <xsl:apply-templates mode="intro"/>
        <xsl:apply-templates mode="content"/>
        <!-- Zpracování všech elementů zdrojového dokumentu -->
      </fo:flow>
    </fo:page-sequence>
  </fo:root>
</xsl:template>


<xsl:template match="a:team" mode="intro">
  <fo:block id="info" text-align="center" font-weight="bold" font-size="24pt"  linefeed-treatment="preserve">
    Basic Information
  </fo:block>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="a:url">
    <fo:block>      
      <xsl:text>
        Project URL: 
      </xsl:text>
    <fo:inline text-decoration="underline">
      <xsl:apply-templates/>
    </fo:inline>
  </fo:block>
</xsl:template>

<xsl:template match="a:members">
  <fo:block>
      <fo:table>
          <fo:table-header>
              <fo:table-row>
                  <fo:table-cell>
                      <fo:block>
                        Members List and Areas:
                      </fo:block>
                  </fo:table-cell>
              </fo:table-row>
          </fo:table-header>

          <fo:table-body>
            <fo:table-row>
                <fo:table-cell border="solid black 1px" padding="2px">
                    <fo:block font-weight="bold">Name:</fo:block>
                </fo:table-cell>

                <fo:table-cell border="solid black 1px" padding="2px">
                    <fo:block font-weight="bold">Email:</fo:block>
                </fo:table-cell>

                <fo:table-cell border="solid black 1px" padding="2px">
                    <fo:block font-weight="bold">Area 1:</fo:block>
                </fo:table-cell>

                <fo:table-cell border="solid black 1px" padding="2px">
                    <fo:block font-weight="bold">Area 2:</fo:block>
                </fo:table-cell>
            </fo:table-row>

            <xsl:apply-templates select="a:member"/>
          </fo:table-body>
        </fo:table>
  </fo:block>
</xsl:template>

<xsl:template match="a:member">
              <fo:table-row>
                  <fo:table-cell border="solid black 1px" padding="2px">
                      <fo:block>
                        <xsl:value-of select="a:name"></xsl:value-of>
                      </fo:block>
                  </fo:table-cell>

                  <fo:table-cell border="solid black 1px" padding="2px">
                      <fo:block>
                          <xsl:value-of select="a:mail"></xsl:value-of>
                      </fo:block>
                  </fo:table-cell>

                  <xsl:apply-templates select="a:areas"/>
              </fo:table-row>
</xsl:template>

<xsl:template match="a:areas">
    <xsl:apply-templates select="a:area"/>
</xsl:template>

<xsl:template match="a:area">
    <fo:table-cell border="solid black 1px" padding="2px">
        <fo:block>
          <fo:basic-link internal-destination="{current()}" text-decoration="underline">
            <xsl:value-of select="current()"/>
          </fo:basic-link>
        </fo:block>
    </fo:table-cell>
</xsl:template>

<xsl:template match="areas" mode="intro">
  <fo:block text-align="center" font-weight="bold" font-size="14pt" linefeed-treatment="preserve">
    &#xA;
    Countries Alphabetically
  </fo:block>
  <xsl:apply-templates select="area:area" mode="intro"/>
</xsl:template>

<xsl:template match="area:area" mode="intro">
  <fo:block>
    <fo:basic-link internal-destination="{area:name}" text-decoration="underline">
      <xsl:value-of select="area:name"/>
    </fo:basic-link>
  </fo:block>
</xsl:template>

<xsl:template match="areas" mode="content">
  <xsl:apply-templates select="area:area"/>
</xsl:template>

<xsl:template match="area:area">
  <fo:block page-break-before="always" id="{area:name}" text-align="center" font-weight="bold" font-size="24pt">
    <xsl:value-of select="area:name"/>
  </fo:block>

  <fo:block>
    <fo:basic-link internal-destination="{area:name}-background" text-decoration="underline">
      Background
    </fo:basic-link>
  </fo:block>   

  <fo:block page-break-after="always">
    <fo:block>
      <fo:basic-link internal-destination="{area:name}-geography" text-decoration="underline">
        Geography
      </fo:basic-link>
    </fo:block>

    <fo:block>
      <fo:basic-link internal-destination="{area:name}-people-and-society" text-decoration="underline">
        People and society
      </fo:basic-link>
    </fo:block>

    <fo:block>
      <fo:basic-link internal-destination="{area:name}-government" text-decoration="underline">
        Government
      </fo:basic-link>
    </fo:block>

    <fo:block>
      <fo:basic-link internal-destination="{area:name}-economy" text-decoration="underline">
        Economy
      </fo:basic-link>
    </fo:block>

    <fo:block>
      <xsl:choose>
        <xsl:when test="area:communications/area:broadcast-media != ''">
          <fo:basic-link internal-destination="{area:name}-communications" text-decoration="underline">
            Communications
          </fo:basic-link>
        </xsl:when>
      </xsl:choose>
    </fo:block>

    <fo:block>
      <xsl:choose>
        <xsl:when test="area:military-and-security/area:military-note != ''">
          <fo:basic-link internal-destination="{area:name}-military-and-security" text-decoration="underline">
            Military and Security
          </fo:basic-link>
        </xsl:when>
      </xsl:choose>
    </fo:block>

    <fo:block>
      <xsl:choose>
        <xsl:when test="area:transportation/area:airports != ''">
          <fo:basic-link internal-destination="{area:name}-transportation" text-decoration="underline">
            Transportation
          </fo:basic-link>
        </xsl:when>
      </xsl:choose>
    </fo:block>
  </fo:block>

  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="area:name"/>

<xsl:template match="area:flag">
  <fo:block>
    <fo:inline>
      <xsl:text>
        Flag:
      </xsl:text>
    </fo:inline>

    <fo:external-graphic src="{current()}" width="25%" content-height="100%" content-width="scale-to-fit" scaling="uniform" border-style="solid" border-width="medium" border-color="black"/>
  </fo:block>
</xsl:template>

<xsl:template match="area:map">
  <fo:block>
    <fo:inline>
      <xsl:text>
        Map:
      </xsl:text>
    </fo:inline>

    <fo:external-graphic src="{current()}"  width="45%" content-height="100%" content-width="scale-to-fit" scaling="uniform" border-style="solid" border-width="medium" border-color="black"/>
  </fo:block>
</xsl:template>

<xsl:variable name="vLower" select=
 "'abcdefghijklmnopqrstuvwxyz'"/>

 <xsl:variable name="vUpper" select=
 "'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

<xsl:template match="area:background">
  <fo:block id="{../area:name}-{local-name()}" text-align="center" font-weight="bold" font-size="14pt" linefeed-treatment="preserve">
    &#xA;
    Background
  </fo:block>

  <fo:block>
    <xsl:value-of select="current()"></xsl:value-of>
  </fo:block>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="
    geo:geography
    | area:people-and-society
    | gov:government
    | area:economy">
  <fo:block id="{../area:name}-{local-name()}" text-align="center" font-weight="bold" font-size="14pt" linefeed-treatment="preserve">
    <xsl:variable name="elementName" select="local-name()"/>
    <xsl:value-of select="
          concat(
            '
            &#xA;
            &#xA;
            &#xA;
            ',
            translate(
              substring(
                $elementName,
                1,
                1),
              $vLower,
              $vUpper
            ),
            substring(
              translate($elementName, '-', ' '),
              2
            )
          )"/>
  </fo:block>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="geo:area-note"/>

<xsl:template match="
    geo:location
  | geo:coordinates
  | geo:map-references
  | geo:area-total
  | geo:area-rank
  | geo:area-comparative
  | geo:land-boundaries
  | geo:coastline
  | geo:climate
  | geo:environment-issues
  | geo:geography-note
  | area:population
  | area:languages
  | area:hiv
  | gov:etymology
  | gov:dependency-status
  | gov:constituion
  | gov:legal-system
  | gov:diplomatic-representation-in-USA
  | gov:diplomatic-representation-from-USA
  | gov:flag-description
  | area:economy-overview
  ">
  <fo:block>
    <fo:inline font-weight="bold">
      <xsl:variable name="elementName" select="local-name()"/>
      <xsl:value-of select="
          concat(
            translate(
              substring(
                $elementName,
                1,
                1),
              $vLower,
              $vUpper
            ),
            substring(
              translate($elementName, '-', ' '),
              2
            )
          )"/>
      : 
    </fo:inline>

    <xsl:apply-templates/>
  </fo:block>
</xsl:template>


<xsl:template match="
  gov:executive-branch
  | gov:judicial-branch
  | gov:national-anthem
  | area:exchange-rates
  | area:broadcast-media
  | area:military-note
  | area:airports
  | area:airports-with-paved-runways
  | area:roadways">
  <fo:block>
    <xsl:choose>
      <xsl:when test="current() != ''">
        <fo:inline font-weight="bold">
          <xsl:variable name="elementName" select="local-name()"/>
          <xsl:value-of select="
              concat(
                translate(
                  substring(
                    $elementName,
                    1,
                    1),
                  $vLower,
                  $vUpper
                ),
                substring(
                  translate($elementName, '-', ' '),
                  2
                )
              )"/>
          : 
        </fo:inline>

        <xsl:apply-templates/>
      </xsl:when>
    </xsl:choose>
  </fo:block>
</xsl:template>


<xsl:template match="gov:country-name">
  <fo:block>
    <fo:inline font-weight="bold">
      Official country name (short / long): 
    </fo:inline>
    <xsl:value-of select="gov:country-name-short"/>
     / 
    <xsl:value-of select="gov:country-name-long"/>
  </fo:block>
</xsl:template>



<xsl:template match="gov:country-name-short"/>

<xsl:template match="gov:country-name-long"/>

<xsl:template match="gov:capital">
  <fo:block>
    <xsl:choose>
      <xsl:when test="gov:capital-name != ''">
          <fo:table>
              <fo:table-header>
                  <fo:table-row>
                      <fo:table-cell>
                          <fo:block font-weight="bold">
                            Capital:
                          </fo:block>
                      </fo:table-cell>
                  </fo:table-row>
              </fo:table-header>

              <fo:table-body>
                <fo:table-row>
                    <fo:table-cell border="solid black 1px" padding="2px">
                        <fo:block font-weight="bold">Name:</fo:block>
                    </fo:table-cell>

                    <fo:table-cell border="solid black 1px" padding="2px">
                        <fo:block font-weight="bold">Coordinates:</fo:block>
                    </fo:table-cell>

                    <fo:table-cell border="solid black 1px" padding="2px">
                        <fo:block font-weight="bold">Time difference:</fo:block>
                    </fo:table-cell>

                    <fo:table-cell border="solid black 1px" padding="2px">
                        <fo:block font-weight="bold">Etymology:</fo:block>
                    </fo:table-cell>
                </fo:table-row>

                <fo:table-row>
                  <fo:table-cell border="solid black 1px" padding="2px">
                    <fo:block>
                      <xsl:value-of select="gov:capital-name"/>
                    </fo:block>
                  </fo:table-cell>

                  <fo:table-cell border="solid black 1px" padding="2px">
                    <fo:block>
                      <xsl:value-of select="gov:capital-coordinates"/>
                    </fo:block>
                  </fo:table-cell>

                  <fo:table-cell border="solid black 1px" padding="2px">
                    <fo:block>
                      <xsl:value-of select="gov:capital-time-difference"/>
                    </fo:block>
                  </fo:table-cell>

                  <fo:table-cell border="solid black 1px" padding="2px">
                    <fo:block>
                      <xsl:value-of select="gov:etymology"/>
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
              </fo:table-body>
          </fo:table>
      </xsl:when>
    </xsl:choose>
  </fo:block>
</xsl:template>

<xsl:template match="area:communications">
  <fo:block id="{../area:name}-communications" text-align="center" font-weight="bold" font-size="14pt" linefeed-treatment="preserve">
    <xsl:choose>
      <xsl:when test="area:broadcast-media != ''">
        &#xA;
        Communications
      </xsl:when>
    </xsl:choose>
  </fo:block>
  <xsl:apply-templates/>
</xsl:template>


<xsl:template match="area:military-and-security">
  <fo:block id="{../area:name}-military-and-security" text-align="center" font-weight="bold" font-size="14pt" linefeed-treatment="preserve">
    <xsl:choose>
      <xsl:when test="area:military-note != ''">
        &#xA;
        Military and Security
      </xsl:when>
    </xsl:choose>
  </fo:block>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="area:transportation">
  <fo:block id="{../area:name}-transportation" text-align="center" font-weight="bold" font-size="14pt" linefeed-treatment="preserve">
    <xsl:choose>
      <xsl:when test="area:airports != ''">
        &#xA;
        Transportation
      </xsl:when>
    </xsl:choose>
  </fo:block>
  <xsl:apply-templates/>
</xsl:template>


</xsl:stylesheet>