<xsl:stylesheet version = '2.0'
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
     xmlns:a="bi-xml-bhkp-team"
     exclude-result-prefixes="a">
     
     <xsl:output method="html" version="5.0"
     encoding="UTF-8" indent="yes"/>

     <xsl:template match="/">
     <xsl:result-document href="output/html/index.html">
        <html lang="en">
            <head>
                <title>BI-XML Countries</title>
                <link rel="stylesheet" type="text/css" media="screen" href="mainPage.css"></link>
            </head>
            <body>
            <div class="page">   
                <h1>BI-XML Countries</h1>
                <p>Tento projekt vznikl jako semestrální práce k předmětu BI-XML v akademickém roce 2019/2020.</p>
                <a class="gitlab-button" href="https://gitlab.fit.cvut.cz/bittnja3/bi-xml">Tento projekt na GitLab →</a>
                <div class="content">
                <div class="team">
                    <p>Členové týmu:</p>    
                    <ul>
                        <xsl:apply-templates select="a:team/a:members/a:member/a:name"/>
                    </ul>
                </div>
                <div class="countries">
                    <p>Zpracované countries:</p>
                    <ul>
                        <xsl:apply-templates select="a:team/a:members/a:member/a:areas/a:area"/>
                    </ul>
                </div>
                </div>
            </div>
            </body>
        </html>
    </xsl:result-document>
    </xsl:template>

    <xsl:template match="a:team/a:members/a:member/a:name">
        <li>
            <xsl:value-of select="."/>
        </li>
    </xsl:template>

    <xsl:template match="a:team/a:members/a:member/a:areas/a:area">
        <xsl:variable name="country">
            <xsl:value-of select="."/>
        </xsl:variable>
        <li>
            <a>
                <xsl:attribute name="href">
                    <xsl:value-of select="concat(translate($country,' ','-'),'.html')"/>
                </xsl:attribute>
                <xsl:value-of select="$country"/>
            </a>
        </li>
    </xsl:template>
</xsl:stylesheet>
