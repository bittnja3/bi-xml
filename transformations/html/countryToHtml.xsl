<xsl:stylesheet version = '2.0'
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
     xmlns:area="bi-xml-bhkp-area"
     exclude-result-prefixes="area">

     <xsl:output method="html" version="5.0"
     encoding="UTF-8" indent="yes"/>

     <xsl:template match="/project/areas">
        <xsl:apply-templates select="//area:area"/>
    </xsl:template>
 
    <xsl:template match="//area:area">

    <xsl:variable name="countryName"> <!-- country name -->
        <xsl:value-of select="./area:name"/>
    </xsl:variable>

    <xsl:result-document href="output/html/{translate($countryName,' ','-')}.html">
        <html lang="en">
            <head>
                <title>
                        <xsl:value-of select="$countryName"/>
                </title>
                <link rel="stylesheet" type="text/css" media="screen" href="country.css"></link>
            </head>
            <body>
                <a id="top"></a> <!-- anchor -->
                <div class="page">

                    <div class="header">
                        <a href="index.html">Back to project main page</a>
                        <h1>
                            <xsl:value-of select="$countryName"/>
                        </h1>
                    </div>

                    <div class="menu">
                        <h2>Jump to:</h2>
                        <ul>
                            <xsl:apply-templates mode="menu"/>
                        </ul>
                    </div>

                    <div class="content">
                        <xsl:apply-templates mode="content"/>
                    </div>

                </div>
                <a class="backToTop" href="#top">Back to top</a>
            </body>
        </html>
    </xsl:result-document> 
    </xsl:template>

    <!-- menu -->
    <xsl:template match="child::*" mode="menu">
        <xsl:variable name="tmp">
            <xsl:value-of select="local-name(.)"/>
        </xsl:variable>
        <xsl:variable name="menu-id">
            <xsl:value-of select="concat(local-name(parent::*),'-',local-name(.))"/>
        </xsl:variable>
        <li>
            <a href="#{$menu-id}">
                <xsl:value-of select="translate($tmp,'-',' ')"/>
            </a>
        </li>
    </xsl:template>

    <!-- content -->
    <xsl:template match="child::*" mode="content"> 
        <xsl:variable name="tmp">
            <xsl:value-of select="translate(local-name(.),'-',' ')"/>
        </xsl:variable>

        <xsl:variable name="firstLetterCapitalTmp">
            <xsl:value-of select="concat(upper-case(substring($tmp,1,1)),
                                    substring($tmp, 2),
                                    ' '[not(last())])"/>
        </xsl:variable>

        <xsl:variable name="menu-id">
            <xsl:value-of select="concat(local-name(parent::*),'-',local-name(.))"/>
        </xsl:variable>

        <xsl:variable name="ancestorsCount">
            <xsl:value-of select="count(ancestor::*)"/>
        </xsl:variable>
        
        <!-- images -->
        <xsl:if test="($tmp = 'map') or ($tmp = 'flag')">
                <h2 id="{$menu-id}">
                        <xsl:sequence select="$firstLetterCapitalTmp"/>
                </h2>
                <img>
                    <xsl:attribute name="src">
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                    <xsl:attribute name="alt">
                        <xsl:value-of select="local-name(.)"/>
                    </xsl:attribute>
                </img>
        </xsl:if>

        <!-- other -->
        <xsl:if test="($tmp != 'map') and ($tmp != 'flag')">
            <xsl:variable name="nodeContents">
                <xsl:value-of select="./text()"/>
            </xsl:variable>
            <xsl:variable name="menu-id">
                <xsl:value-of select="concat(local-name(parent::*),'-',local-name(.))"/>
            </xsl:variable>

            <xsl:if test="string-length($nodeContents) &gt; 0">
                <xsl:choose>
                    <xsl:when test="$ancestorsCount=3">
                        <h2 id="{$menu-id}">
                            <xsl:sequence select="$firstLetterCapitalTmp"/>
                        </h2>
                        <xsl:copy-of select="normalize-space($nodeContents)"/>
                    </xsl:when>
                    <xsl:when test="$ancestorsCount=4">
                        <div class="underSection">
                            <h3 id="{$menu-id}">
                                <xsl:sequence select="$firstLetterCapitalTmp"/>
                            </h3>
                            <xsl:copy-of select="normalize-space($nodeContents)"/>
                        </div>
                    </xsl:when>
                    <xsl:when test="$ancestorsCount=5">
                        <div class="underUnderSection">
                            <h4 id="{$menu-id}">
                                <xsl:sequence select="$firstLetterCapitalTmp"/>
                            </h4>
                            <xsl:copy-of select="normalize-space($nodeContents)"/>
                        </div>
                    </xsl:when>
                </xsl:choose>
                <xsl:apply-templates select="child::*" mode="content"/> 
            </xsl:if>                
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>